let user = require( '../user' );

module.exports.loginRequest = async function ( req , res ) {
    if ( req.body.email && req.body.password ) {
        req.db.collection( 'user' ).findOne( { email: req.body.email , password: req.body.password } , function ( error , user ) {
            //console.log(req.session)
            if ( error ) {
                return res.sendStatus( 500 );
            } else {
                if ( user == null ) {
                    return res.render( 'login' , { error: 'Неверный логин или пароль' } );
                } else {
                    req.session.username = req.body.email;
                    req.session.userName = user.name;
                    req.session.tsUser = req.body.email;
                    req.session.userId = user._id;
                    req.session.validated = true;
                    return res.redirect( '/items/' );
                }
            }
        });
    } else {
        return res.render( 'login' , { error: 'Неверный логин или пароль' } );
    }
}