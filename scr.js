const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const dom = new JSDOM(``, {
  url: "https://www.ozon.ru/context/detail/id/142719504/",
  referrer: "https://www.ozon.ru/",
  contentType: "text/html",
  includeNodeLocations: true,
  storageQuota: 10000000
});