let user = require( '../user' );

module.exports.registerRequest = async function ( req , res ) {
    if (req.body.name && req.body.lastname && req.body.email && req.body.password ) {
        if(req.body.password != req.body.passwordcopy){
            return res.render('register', { error: 'Введенные пароли не совпадают' });
        }
        if (!req.body.check) {
            return res.render('register', { error: 'Не приняты условия' });
        }
        let user = await req.db.collection( 'user' ).findOne( { email: req.body.email } );
        if ( null !== user ) {
            return res.render( 'register' , { error: 'Пользователь с такой эл. почтой существует' } );
        }
        req.db.collection('user').insertOne({ name: req.body.name, lastname: req.body.lastname, email:req.body.email , password:req.body.password } , function ( error , user ) {
            if ( error ) {
                return res.sendStatus( 500 );
            } else {
                req.session.username = req.body.email;
                req.session.tsUser = req.body.email;
                req.session.userName = user.name + " " + (user.lastname || "");
                req.session.userId = user.ops[ 0 ]._id;
                req.session.validated = true;
                return res.redirect( '/items/' );
            }
        });
    } else {
        return res.render( 'register' , { error: 'Проверьте вводимые данные' } );
    }
}