$(document).ready(function () {
    if ($(window).width() >= 1024) {
        $('.reviews_slider').slick({
            appendArrows: $('.reviews_header_arrows'),
            infinite: true,
            slidesToShow: 3,
            autoplay: true,
            prevArrow: '<button type="button" class="arr arr_prev"><div></div></button>',
            nextArrow: '<button type="button" class="arr arr_next"><div></div></button>',
        })
    }
    $('.reviews_slider').on('afterChange', function (e, slick, currentSlide) {
        $('.slick-current').addClass('.reviews_slide_act');
    })
    $('.links_on_page a').on('click', function (e) {
        var clickTo = $(e.target).attr('href');
        $('html,body').stop().animate({
            scrollTop: $(clickTo).offset().top - $('.menu_content').outerHeight() + 1
        }, 800);
        e.preventDefault();
        if ($(window).width() < 1024){
            $('.hamburger').removeClass('is-active');
            $('.menu_mobile').removeClass('menu_mobile_active');
            toggleLogo();
        }

    });
    $('#more').click(function (e) {
        $('.reviews_slide').removeClass('reviews_hidden');
        $(this).css({
            'display': 'none'
        });
    })
    $('#more_footer').click(function (e) {
        $('.footer_block').removeClass('footer_hidden');
        $(this).css({
            'display': 'none'
        });
    })

    $('.hamburger').click(function (e) {
        $(this).toggleClass('is-active');
        $('.menu_mobile').toggleClass('menu_mobile_active');
        toggleLogo();
    })

    function toggleLogo() {
        $('.menu_logo2').toggleClass('menu_logo_hid');
        $('.menu_logo1').toggleClass('menu_logo_hid');
        $('body').toggleClass('non_scroll');
    }


    var trues = [];
    $('.login_from input').each(function( index ) {
        trues[index] = false;
    });
    $('.login_from button[type="submit"]').click(function (e){
        console.log(trues)
        if(!checkAll()){
            e.preventDefault();
        }
    })
    $('.login_from input').on('input', function(e) {
        var val = $(this).val();
        var type = $(this).attr('data-val');
        if(type === 'email'){
            valEmail(val, this);
        } else if (type === 'pass') {
            valPass(val, this);
        } else if (type === 'name' ){
            if (testPass(val, 2)){
                $(this).removeClass('true_inp')
                $(this).addClass('false_inp')
                trues[4] = false;
            } else{
                $(this).removeClass('false_inp')
                $(this).addClass('true_inp')
                trues[4] = true;
            }
        } else if (type === 'check'){
            if (!$(this).prop("checked")){
                trues[3] = false;
            } else{
                trues[3] = true;
            }
        } else if (type === 'passcopy'){
            if(val !== $('.log_inp[data-val="pass"]').val()){
                $(this).removeClass('true_inp')
                $(this).addClass('false_inp')
                trues[2] = false;
            } else{
                $(this).removeClass('false_inp')
                $(this).addClass('true_inp')
                trues[2] = true;
            }
        }
    });
    function valEmail(val, thisBlock){
        if (!testMail(val)){
            $(thisBlock).removeClass('true_inp')
            $(thisBlock).addClass('false_inp')
            trues[0] = false;
        } else{
            $(thisBlock).removeClass('false_inp')
            $(thisBlock).addClass('true_inp')
            trues[0] = true;
        }
    }
    function valPass(val, thisBlock){
        if (testPass(val, 6)){
            $(thisBlock).removeClass('true_inp')
            $(thisBlock).addClass('false_inp')
            trues[1] = false;
        } else{
            $(thisBlock).removeClass('false_inp')
            $(thisBlock).addClass('true_inp')
            trues[1] = true;
        }
    }
    function testPass(val, large){
        return val.length < large;
    }
    function testMail(val){
        return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(val)
    }
    function checkAll(){
        for (form in trues){
            if(!trues[form]) {
                return false;
            }
        }
        return true
    }
});