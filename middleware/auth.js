const ObjectID = require( 'mongodb' ).ObjectID;
module.exports.auth = async function( req , res , next ) {
    req.db.collection( 'user' ).findOne( { _id: ObjectID( req.session.userId ) } , function ( error , user ) {
        //console.log(user)
        if ( error ) {
            console.log(error)
            return next( error );
        } else {      
            if ( user === null ) {
                return res.redirect( '/login' );
            } else {
                return next();
            }
        }
    });
}