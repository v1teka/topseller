const ObjectID = require( 'mongodb' ).ObjectID;
module.exports.guest = async function( req , res , next ) {
    if ( null == req.session.userId || undefined == req.session.userId ) return next();
    req.db.collection( 'user' ).findOne( { _id: ObjectID( req.session.userId ) } , function ( error , user ) {
        if ( error ) {
            console.log(error)
            return next( error );
        } else {      
            if ( user !== null ) {
                return res.redirect( '/items/' );
            } else {
                console.log(user)
                return next();
            }
        }
    });
}