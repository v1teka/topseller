const gulp = require('gulp'),
      sass = require('gulp-sass'),
      uglify = require('gulp-uglify'),
      autoprefixer = require('gulp-autoprefixer'),
      rename = require("gulp-rename"),
      minifyCSS = require('gulp-minify-css'),
      concatCss = require('gulp-concat-css');

gulp.task('scss', function () {
  return gulp.src('dist/scss/*.scss')
    .pipe(sass())
    .pipe(concatCss("style.css"))
    .pipe(minifyCSS())
    .pipe(rename('style.min.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest('app'));
});

gulp.task('fonts', function () {
    return gulp.src('dist/fonts/*.*')
      .pipe(gulp.dest('app/fonts'));
});

gulp.task('default', function () {
    return gulp.src('dist/*.html')
      .pipe(gulp.dest('app/'));
});

gulp.task('img', function () {
    return gulp.src('dist/img/*.*')
      .pipe(gulp.dest('app/img'));
});

gulp.task('js', function () {
    return gulp.src('dist/js/main.js')
      .pipe(uglify())
      .pipe(rename('main.min.js'))
      .pipe(gulp.dest('app/js'));
});

gulp.task('watch', function (){
    gulp.watch("dist/scss/*.scss", gulp.parallel('scss'))
    gulp.watch("dist/*.html", gulp.parallel('default'))
    gulp.watch("dist/img/*.*", gulp.parallel('img'))
    gulp.watch("dist/fonts/*.*", gulp.parallel('fonts'))
    gulp.watch("dist/js/main.js", gulp.parallel('js'))
});