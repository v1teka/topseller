const chromeLauncher = require('chrome-launcher');
const CDP = require('chrome-remote-interface');

(async function () {
    async function launchChrome() {
        return await chromeLauncher.launch({
            chromeFlags: [
                '--disable-gpu',
                '--headless'
            ]
        });
    }
    const chrome = await launchChrome();
    const protocol = await CDP({
        port: chrome.port
    });

    const {
        DOM,
        Page,
        Emulation,
        Runtime
    } = protocol;
    await Promise.all([Page.enable(), Runtime.enable(), DOM.enable()]);

    Page.navigate({
        url: 'https://www.ozon.ru/context/detail/id/160543700/'
    }).then(()=> {
        var loaded = false;
        
        var loop = setInterval(async () => {
            const script1 = "document.querySelector('.top-seller-block .column > div >  div> div>div >  a').textContent"
            // Evaluate script1
            Runtime.evaluate({
                expression: script1
            })
                .then(result => {
                    if(!result || !result.result || !result.result.value){
                        console.log("not loaded!");
                        return;
                    }
                    console.log(result.result.value);
                    clearInterval(loop);
                    protocol.close();
                    chrome.kill();
                })
                .catch(error => {
                    console.log(error);
                });

        }, 5000);
    });
})();