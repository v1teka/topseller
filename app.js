let express = require("express");
let path = require("path");
let bodyParser = require("body-parser");
let user = require('./user');
let exprSession = require('express-session');
let importjs = require('./import');
let goods = require('./goods');
let session = require('./session');
let auth = require('./middleware/auth').auth;
let authAdmin = require('./middleware/authAdmin').authAdmin;
let authService = require('./middleware/authService').authService;
let guest = require('./middleware/guest').guest;
let registerRequest = require('./requests/register').registerRequest;
let loginRequest = require('./requests/login').loginRequest;
let expressMongoDb = require('express-mongo-db');
let swig = require('swig');


const app = express();
const import_route = require('./routes/import.route');
const reports_route = require('./routes/report.route');
const request = require('request');
const chromeLauncher = require('chrome-launcher');
const CDP = require('chrome-remote-interface');
const fs = require('fs');
const yandexCheckout = require('yandex-checkout')('649384', 'live_PSPk8KVXARyqNiJLVKIl_TSwKDcHp9eG38gD91kyUeo');

const config = {
  dbURL: 'mongodb://dmi:dmi123@ds317808.mlab.com:17808/freelance101',
  sellerCSSselector: '.top-seller-block .column > div > div > div> div a, .top-seller-block .column div + span, .top-seller-block div > div > span:only-child, [data-widget="webCurrentSeller"] :nth-child(2) [href^="/seller/"], [data-widget="webCurrentSeller"] div div > span',
  priceCSSselector: '.top-sale-block > div > div > div >  div > div > div> div>span:first-child, .top-sale-block > div > div div > div > div> div>span:first-child',
  abroadCSSselector: '.top-sale-block div > span > a:first-child',
  minutes: 1,
  autoStart: true,
  timeout_seconds: 20,
  MaxLogMessages: 300,
  orderStatus: {
    initial: 'initial',
    pending: 'pending',
    succeeded: 'succeeded',
    canceled: 'canceled',
  },
  messageStatus: {
    success: 'success',
    message: 'message',
    error: 'error',
    warning: 'warning'
  },
  subscription:{
    daysCount: 30,
    testDaysCount: 7,
  },
  secret: 'topse11er'
};
const autorepricers = [];

class Autorepricer {
  constructor(username, autorepricer_run) {
    this.status = 0;
    this.db = null;
    this.chrome = {
      browser: null,
      Page: null,
      Runtime: null,
      start: () => new Promise((res, rej) => {
        CDP({
          port: this.chrome.browser.port
        }).then((protocol) => {
          const {
            DOM,
            Page,
            Emulation,
            Runtime
          } = protocol;
          Promise.all([Page.enable(), Runtime.enable(), DOM.enable()])
              .then(() => {
                //console.log("Браузер для " + this.username + " запущен");
                this.chrome.Page = Page;
                this.chrome.Runtime = Runtime;
                return res(true);
              })
              .catch(() => {
                //console.log("Ошибка запуска браузера для " + this.username);
                this.chrome.close();
                return res(false);
              });
        }).catch(() => {
          //console.log("Can't initialize chrome protocol");
          this.chrome.close();
          return res(false);
        });
      }),
      close: async () => {
        try {
          await this.chrome.browser.kill();
        } catch{ }

        delete this.chrome.browser;
      }
    };

    this.username = username;

    if (autorepricer_run)
      this.init();
  };

  getLinkData(itemLink) {
    return new Promise((resolve, reject) => {
      let { Page, Runtime } = this.chrome;
      let newItem = { link: itemLink, seller: null, price: null, abroad: false };
      try {
        Page.navigate({
          url: itemLink
        });
      } catch{
        //console.log("unreachable URL given: ", itemLink);
        return resolve(newItem);
      }
      var i = 0;
      var loop = setInterval(async () => {
        if (i > config.timeout_seconds) {
          console.error("Parsing timeout on " + itemLink);
          clearInterval(loop);
          return resolve(newItem);
        }

        var sellerText, priceText, abroadText;
        var script = "document.querySelector('" + config.sellerCSSselector + "').textContent";
        try {
          sellerText = await Runtime.evaluate({
            expression: script
          });
        } catch (error) {
          //console.log(error);
        }
        if (!sellerText || !sellerText.result || !sellerText.result.value) {
          process.stdout.write(".");
          i++;
          return;
        }

        clearInterval(loop);

        newItem.seller = sellerText.result.value.trim().replace(/\r?\n/g, "");
        script = "document.querySelector('" + config.priceCSSselector + "').textContent";
        try {
          priceText = await Runtime.evaluate({
            expression: script
          });
          if (priceText.result.value)
            newItem.price = Number(priceText.result.value.replace(/\D+/g, ""));
        } catch (error) {
          //console.log(error);
        }


        script = "document.querySelector('" + config.abroadCSSselector + "').textContent";

        try {
          abroadText = await Runtime.evaluate({
            expression: script
          });

          newItem.abroad = Boolean(abroadText.result && abroadText.result.value && abroadText.result.value.length);
        } catch (error) {
          //console.log(error);
        }


        return resolve(newItem);

      }, 1000);
    });
  };

  async run() {
    let db = this.db;
    if (db == null || this.chrome.browser == null) {
      //console.log("Авторепрайсер пользователя " + this.username + " не инициализирован");
      this.init();
      return;
    }

    let priceChangeCount = 0;

    let browserStarted = await this.chrome.start();
    if (!browserStarted) {
      //console.log("Запускаем браузер для пользователя " + this.username);
      this.init();
      return;
    }

    var user = await db.collection('user').findOne({ email: this.username });
    if (!user) console.error("user not found: " + this.username);

    const userSubscription = await db.collection('subscriptions').findOne({_id:user.subscription_id});
    let maxArtCount = userSubscription.maxArtCount || 0;

    //console.log("Авторепрайсинг товаров пользователя ", this.username);
    logIntoFile(this.username, "Начинаем мониторинг цен. "+((maxArtCount)? "Максимальное число товаров: "+maxArtCount+" (тариф "+userSubscription.name+")":"Ограничения по количеству нет (подписка "+userSubscription.name+")"), config.messageStatus.success);

    var ourItems = await db.collection('ozonItems').find({ tsUser: user.email, active: true })
        .limit(maxArtCount).toArray();
    await asyncForEach(ourItems, async ourItem => {

      //console.log("Парсинг по товару", ourItem.name);
      logIntoFile(user.email, "Парсинг по товару " + ourItem.name);
      var [error, res, body] = await ozonRequest('/v1/product/info', user.ozonClientId, user.ozonApiKey,
          {
            "offer_id": ourItem.art
          });
      if (body.error) {
        console.error("Артикул", ourItem.art, body.error.message);
        return true;
      }

      let ourCurrentPrice = Number(body.result.price);

      let minPriceItem = { seller: null, price: null };

      var linksData = [];
      var links = ourItem.links.split('\n');

      await asyncForEach(links, async link => {
        let linkData = await this.getLinkData(link);
        linksData.push(linkData);
      });
      await asyncForEach(linksData, async linkData => {
        if (linkData.price && linkData.seller) {
          if (linkData.seller.toLowerCase() != user.sellerName.toLowerCase()) {
            if (minPriceItem.price && linkData.price > minPriceItem.price) {
              //console.log("[" + user.email + "][" + linkData.seller + "] " + linkData.price + " > " + minPriceItem.price);
              logIntoFile(user.email, "[" + linkData.seller + "] " + linkData.price + " > " + minPriceItem.price);
            } else if (linkData.abroad && !ourItem.abroad) {
              //console.log("[" + user.email + "][" + linkData.seller + "] цена игнорируется (доставка из-за рубежа)");
              logIntoFile(user.email, "[" + linkData.seller + "] цена игнорируется (доставка из-за рубежа)");
            } else {
              if (minPriceItem.seller == null || minPriceItem.price == null) {
                //console.log("[" + user.email + "][" + linkData.seller + "] Найдена цена " + linkData.price);
                logIntoFile(user.email, "[" + linkData.seller + "] Найдена цена " + linkData.price);
              } else {
                //console.log("[" + user.email + "][" + linkData.seller + "] " + linkData.price + " <= " + minPriceItem.price);
                logIntoFile(user.email, "[" + linkData.seller + "] " + linkData.price + " <= " + minPriceItem.price);

              }
              minPriceItem.price = linkData.price;
              minPriceItem.seller = linkData.seller;
            }
          } else {
            //console.log("[" + user.email + "][" + user.sellerName + "] " + linkData.price + " (наша цена)");
            logIntoFile(user.email, "[" + user.sellerName + "] " + linkData.price + " (наша цена)");
          }
        }
        else {
          //console.log("[" + user.email + "] Seller:", linkData.seller, "; price:", linkData.price, ". Check product at " + linkData.link);
          logIntoFile(this.username, "Не найден товар по адресу " + linkData.link, config.messageStatus.warning);
        }
      });

      let actualPrice = minPriceItem.price;
      let ourMinPrice = ourItem.minPrice;

      let itemInBuyBox = false;
      if (minPriceItem.seller) {
        //console.log("Товар '" + ourItem.name + "'. Минимальная цена :", actualPrice, "(", minPriceItem.seller, "),  наша минимально-допустимая", ourMinPrice);
        logIntoFile(user.email, "[" + minPriceItem.seller + "] Минимальная найденная цена - " + actualPrice);
        let newPrice = actualPrice;
        if (!ourItem.hold) newPrice--;
        else logIntoFile(user.email, "Новая цена будет равна минимальной (опция \"Держать цену\" включена)");

        itemInBuyBox = Boolean(newPrice >= ourMinPrice);

        if (!itemInBuyBox) {
          //console.log("Не меняем цену товара " + ourItem.name + " на " + newPrice + " т.к. она ниже допустимого");
          logIntoFile(user.email, "Не меняем цену товара " + ourItem.name + " на " + newPrice + " т.к. она ниже допустимого");
        }
        else if (ourCurrentPrice != newPrice) {
          priceChangeCount++;
          //console.log("Меняем цену товара " + ourItem.name + " на " + newPrice);
          logIntoFile(user.email, "Меняем цену товара " + ourItem.name + " на " + newPrice, config.messageStatus.success);
          this.setPrice(ourItem.art, newPrice, user);
        } else {
          logIntoFile(user.email, "Текущая цена " + ourCurrentPrice + " не меняется");
        }
      } else {
        //console.log("По товару " + ourItem.name + " не найдено чужих цен");
        logIntoFile(user.email, "По товару " + ourItem.name + " не найдено чужих цен");
        if (ourItem.maxPrice && Number(ourItem.maxPrice) != ourCurrentPrice) {
          console.log(ourItem.maxPrice + '<>' + ourCurrentPrice);
          itemInBuyBox = true;
          priceChangeCount++;
          logIntoFile(user.email, "Устанавливаем свою максимальную цену на " + ourItem.name, config.messageStatus.success);
          this.setPrice(ourItem.art, ourItem.maxPrice, user);
        }
        else this.setPrice(ourItem.art, ourCurrentPrice, user, false);
      }
      this.setBuyBox(ourItem.art, user, itemInBuyBox);
    });

    this.addPriceChangeCount(priceChangeCount);
    this.nextLoop();
  };

  nextLoop() {
    this.dispose();
    //console.log('Авторепрайсинг завершён. Следующий запуск через ' + config.minutes + ' минут');
    logIntoFile(this.username, 'Авторепрайсинг завершён. Следующий запуск через ' + config.minutes + ' минут', config.messageStatus.success);
    this.timer = setTimeout(() => { this.init() }, config.minutes * 60 * 1000);
  };

  async addPriceChangeCount(count){
    this.db.collection('counters').updateOne({name: 'priceChange'},
        {
          $inc: {
            value: count
          }
        });
  };

  async trigger(currentStatus) {
    if (currentStatus) {
      //console.log("Пользователь " + this.username + " отключил авторепрайсер");
      logIntoFile(this.username, "Авторепрайсер отключен", config.messageStatus.warning);
      this.dispose();
    } else {
      //console.log("Пользователь " + this.username + " запустил авторепрайсер");
      logIntoFile(this.username, "Авторепрайсер запущен", config.messageStatus.success);
      this.init();
    }
  };

  dispose() {
    clearTimeout(this.timer);
    this.chrome.close();
    if (this.db)
      this.db.close();
    delete this.db;
  };

  hideItem(product_id, clientID, apiKey) {
    ozonRequest('/v1/product/deactivate', clientID, apiKey,
        {
          "product_id": product_id
        })
        .then((result) => {
          let [error, ozonRes, body] = result;
          if (body.error) {
            console.error("Error hide item " + product_id, body.error.message);
            return;
          }

          //console.log("Hide product " + product_id, body.result);
        });
  };

  setBuyBox(itemArt, user, buybox) {
    this.db.collection('ozonItems').update({
          tsUser: user.email,
          art: itemArt
        },
        {
          $set: { buybox }
        });
  };

  setPrice(itemArt, price, user, setOzonPrice = true) {
    this.db.collection('ozonItems').update({
          tsUser: user.email,
          art: itemArt
        },
        {
          $set: { currentPrice: price }
        });

    if (!setOzonPrice) return;

    var clientID = user.ozonClientId,
        apiKey = user.ozonApiKey;

    ozonRequest('/v1/product/import/prices', clientID, apiKey,
        {
          "prices": [
            {
              "offer_id": String(itemArt),
              "price": String(price),
              "vat": "0"
            }
          ]
        }).then(
        (result) => {
          let [err, response, body] = result;
          if(err && err.length){
            console.error("Error #1 set price of ", itemArt, " below");
            console.error(err);
            return;
          }
          if (!body) {
            console.error("Error #2 set price of ", itemArt, ". no 'body'");
            return;
          }
          if (body.error) {
            console.error("Error #3 set price of ", itemArt);
            if (body)
              console.error(body.error.message);
            return;
          }
          //if (body.result[0].updated)
          //  console.log(itemArt, " price has been set to ", price);
          //else
          //  console.log(itemArt, " Error #3 set price: ", body.result[0].errors[0].message);
        });
  };

  async init() {
    //console.log("Инициализируем авторепрайсер пользователя " + this.username);
    let hasAccess = await this.dbConnect();
    if(!hasAccess){
      //console.log("["+this.username+"] Ошибка подключения к БД");
      return;
    }

    let userLevel = await userSubscriptionLevel(this, this.username);
    if(userLevel < 1){
      logIntoFile(this.username, "Подписка истекла или не подключена. Запуск Авторепрайсера отменён.", config.messageStatus.error)
      //console.log("[" + this.username + "] Подписки нет");
      return userAutoPayment(this, this.username);
    }

    //console.log("["+this.username+"] Подписка проверена");
    let chromeInitialized = await this.chromeInit();
    if (!chromeInitialized) {
      //console.log("[" + this.username +"] Ошибка запуска брузера");
      return;
    }

    this.run();
  };

  dbConnect(){
    return new Promise((res, rej) => {
      //console.log("["+this.username+"] Получаем данные из БД");

      let getAccess = () => {
        //console.log("[" + this.username +"] Данные из БД получены. Проверяем подписку");
        res(true);
      };

      try {
        expressMongoDb(config.dbURL)(this, null, getAccess);
      }
      catch (error) {
        console.error(error);
        res(false);
      }
    });
  };

  chromeInit(){
    return new Promise((res, rej) => {
      if (this.chrome && this.chrome.browser) {
        //console.log("Браузер уже создан (" + this.username + ")");
        return res(true);
      }
      //console.log("Создаём headless-браузер (" + this.username + ")");
      chromeLauncher.launch({
        chromeFlags: [
          '--disable-gpu',
          '--headless',
          '--no-sandbox'
        ]
      }).then((chrome) => {
        this.chrome.browser = chrome;
        return res(true);
      }).catch(rej);
    });
  };
};


// let sessions;

app.use(expressMongoDb(config.dbURL));
app.use(express.static("public"));
app.use(exprSession({
  secret: config.secret
}));
app.use('/collect', import_route);
app.use('/report', reports_route);

// SWIG

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', `${__dirname}/views`);
app.set('view cache', false);
swig.setDefaults({
  cache: false
});

//SWIG

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());






// AUTHORIZATION

app.get('/login', guest, function (req, res) {
  res.render('login');
});
app.get('/register', guest, function (req, res) {
  res.render('register');
});
app.post('/register', guest, function (req, res) {
  registerRequest(req, res);
});
app.post('/login', guest, function (req, res) {
  loginRequest(req, res);
});
app.get('/logout', auth, function (req, res) {
  req.session.destroy;
  req.session.destroy();
  return res.redirect('/login');
});

// AUTHORIZATION






// ADMIN

app.get('/users', authAdmin, async function (req, res) {
  let resp = await req.db.collection('user').find({}).toArray();
  await resp.map(async user => {
    if (user.subscription_id) {
      user.subscription_date = user.subscription_expire_date.toISOString().split('T')[0];
      user.subscription_name = await req.db.collection('subscriptions').findOne({ _id: user.subscription_id }).name;
    }
  })
  res.render('users', {
    mainInfo: req.session,
    users: resp,
  });
});

// ADMIN






// TESTING

app.get('/all-users', async function (req, res) {
  let resp = await req.db.collection('user').find({}).toArray();
  //console.log(resp)
  res.json(resp);
});

app.get('/all-items', async function (req, res) {
  let resp = await req.db.collection('ozonItems').find({}).toArray();
  res.json(resp);
});

app.get('/ozonclientdata', async function (req, res) {
  const {token, user_email} = req.query;
  
  if (token != config.secret)
    return res.status(403).end('Access denied');

  let resp = await req.db.collection('user').findOne({
    email: user_email
  });

  if(resp == null)
    return res.status(404).end('{}');

  let clientOzonData = {
    ozonClientId: resp.ozonClientId,
    ozonApiKey: resp.ozonApiKey
  }
  res.json(clientOzonData);
});

// TESTING






// OZON

app.get('/items', auth, async function (req, res) {
  let resp = await req.db.collection('ozonItems').find({
    tsUser: req.session.tsUser
  }).toArray();

  let userInfo = await getUserInfo(req);

  let items = resp.map(item => {
    item.links = item.links.split('\n')
        .filter(link => link.trim().length)
        .map(link => {
          let substr = link.match(/\/detail\/id(\/\d+\/)/);
          if (substr && substr[1] && substr[1].length > 0) return { address: link, name: substr[1] };
          else return { address: link, name: "(ссылка)" };
        });

    item.currentPrice = (item.currentPrice) ? Number(item.currentPrice).toFixed() : 0;
    let commission = item.commission || 0;
    let income = item.currentPrice * (100 - commission) / 100;
    item.profit = Number(income - Number(item.cost)).toFixed(2);
    item.profitPercent = Number((income - Number(item.cost)) / item.cost * 100).toFixed();
    return item;
  });

  req.session.sellerName = userInfo.sellerName;
  userInfo.canAddItems = await userCanAddItems(req, userInfo);

  res.render('items/list', {
    mainInfo: req.session,
    userInfo,
    items,
    currentTab: req.query.tab || "",
    triggerOn: (items.filter(item => !item.active).length < 1),
  });
});

app.get('/items/edit', auth, async (req, res) => {
  let canAddItems = await userCanAddItems(req);
  if (!canAddItems) {
    return res.redirect('/subscription');
  }

  let userInfo = await getUserInfo(req);
  res.render('items/edit', {
    mainInfo: req.session,
    userInfo
  });
});

app.post('/items/edit', auth, async function (req, res) {
  const user = await req.db.collection('user').findOne({
    email: req.session.tsUser
  });

  let canAddItems = await userCanAddItems(req, user);
  if(!canAddItems){
    logIntoFile(user.email, "Ошибка добавления товара: достигнуто максимальное количество артикулов", config.messageStatus.error);
    return res.redirect('/items/');
  }

  const {
    art,
    name,
    cost,
    minPrice,
    maxPrice,
    commission,
    links,
  } = req.body;

  let check = await req.db.collection('ozonItems').findOne({
    tsUser: req.session.tsUser,
    art
  });

  if (check != null) {
    //console.log("Ozon товар с таким именем уже существует");
    return res.redirect('/items/');
  }
  let currentPrice = 0;

  let [error, ozonRes, body] = await ozonRequest('/v1/product/info', user.ozonClientId, user.ozonApiKey,
      {
        "offer_id": art
      });
  if (body && !body.error)
    currentPrice = body.result.price;

  let resp = await req.db.collection('ozonItems').insertOne({
    tsUser: req.session.tsUser,
    art,
    name: name || "",
    cost,
    minPrice,
    maxPrice,
    currentPrice,
    commission,
    links,
  });

  return res.redirect('/items/');
});


app.get('/items/edit/:art', auth, async (req, res) => {
  const { art } = req.params;

  let resp = await req.db.collection('ozonItems').findOne({
    tsUser: req.session.tsUser,
    art,
  });

  let userInfo = await getUserInfo(req);

  res.render('items/edit', {
    mainInfo: req.session,
    userInfo,
    item: resp
  });
});

app.post('/items/edit/:art', auth, async function (req, res) {
  const oldArt = req.params.art;
  const {
    art,
    name,
    cost,
    minPrice,
    maxPrice,
    commission,
    links,
  } = req.body;

  let check = await req.db.collection('ozonItems').findOne({
    tsUser: req.session.tsUser,
    art: oldArt
  });


  if (check == null) {
    //console.log("Товар '", oldArt, "' не найден в базе");
  } else {
    let currentPrice = -1;

    let user = await req.db.collection('user').findOne({
      email: req.session.tsUser,
    });

    let [error, res, body] = await ozonRequest('/v1/product/info', user.ozonClientId, user.ozonApiKey,
        {
          "offer_id": art
        });
    if (body && !body.error)
      currentPrice = body.result.price;

    let resp = await req.db.collection('ozonItems').update({
      tsUser: req.session.tsUser,
      art: oldArt
    }, {
      $set: {
        art,
        name: name || "",
        cost,
        minPrice,
        maxPrice,
        currentPrice,
        commission,
        links,
      }
    });
  }
  return res.redirect('/items/');
});

app.delete('/items/edit', auth, async function (req, res) {
  const { art } = req.body;

  let result = await req.db.collection('ozonItems').deleteOne({
    tsUser: req.session.tsUser,
    art
  });
  if (result == null) {
    //console.log("Товар арт." + art, "не найден в базе");
  } else {
    //console.log("Товар арт." + art + "был удален");
  }

  return res.status(200).end();
});


app.post('/items/toggle', auth, async function (req, res) {
  const {
    art,
    active
  } = req.body;

  /*
  let maxArtCount = await userMaxArtCount(user._id, req);
  let activeCount = await req.db.collection('ozonItems').find({
    tsUser: req.session.tsUser,
    active: true
  }).toArray().length;

  if(maxArtCount < activeCount+1){
    console.log(req.session.tsUser+" максимальное количество активных товаров достигнуто");
    active = false;
  }*/

  req.db.collection('ozonItems').findOneAndUpdate({
        tsUser: req.session.tsUser,
        art,
      },
      { $set: { active, buybox:false } },
      { upsert: false, returnNewDocument: true }
  ).then(result => {
        if (result == null) {
          return res.status(404).end();
        } else {
          //console.log(result);
          return res.end(JSON.stringify({ active }));
        }
      }
  ).catch(error => {
    //console.log(error);
    return res.status(400).end();
  });
});

app.post('/items/abroad', auth, async function (req, res) {
  const {
    art
  } = req.body;

  let item = await req.db.collection('ozonItems').findOne({
    tsUser: req.session.tsUser,
    art,
  });

  if (item == null)
    return res.status(404).end();

  req.db.collection('ozonItems').findOneAndUpdate({
        tsUser: req.session.tsUser,
        art,
      },
      { $set: { abroad: !item.abroad } },
      { upsert: false }
  ).then(result => {
        if (result == null) {
          return res.status(404).end();
        } else {
          //console.log(result);
          return res.end(JSON.stringify({ abroad: !result.value.abroad }));
        }
      }
  ).catch(error => {
    //console.log(error);
    return res.status(400).end();
  });
});

app.post('/items/hold', auth, async function (req, res) {
  const {
    art
  } = req.body;

  let item = await req.db.collection('ozonItems').findOne({
    tsUser: req.session.tsUser,
    art,
  });

  if (item == null)
    return res.status(404).end();

  req.db.collection('ozonItems').findOneAndUpdate({
    tsUser: req.session.tsUser,
    art,
  },
    { $set: { hold: !item.hold } },
    { upsert: false }
  ).then(result => {
    if (result == null) {
      return res.status(404).end();
    } else {
      //console.log(result);
      return res.end(JSON.stringify({ hold: !result.value.hold }));
    }
  }
  ).catch(error => {
    //console.log(error);
    return res.status(400).end();
  });
});

// OZON






// AUTOREPRICER

app.get('/autorepricer/log', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);
  res.render('autorepricer/log', {userInfo});
});

app.get('/getlog', auth, async (req, res) => {
  let userInfo = await req.db.collection('user').findOne({ email: req.session.tsUser });

  var logMessages = [];
  let userlogfile = userLogFileName(userInfo.email)

  if (fs.existsSync(userlogfile)) {
    logMessages = fs.readFileSync(userlogfile, 'utf8');
    logMessages = logMessages.split('\n');
    logMessages.splice(0 , logMessages.length - config.MaxLogMessages);
    logMessages.splice(logMessages.length-1);
  }

  res.set('Content-Type', 'application/json');
  res.set('Charset', 'utf-8');
  res.end(JSON.stringify(logMessages));
});

app.get('/runarp', auth, async (req, res) => {
  let user = await req.db.collection('user').findOne({
    email: req.session.tsUser
  });

  res.set('Content-Type', 'application/json');
  res.set('Charset', 'utf-8');
  let responseJSON = {success: false, status: (user.autorepricer_run)? 'active': 'paused'};
  let autorepricerStatus = user.autorepricer_run;
  req.db.collection('user').update({
        email: req.session.tsUser,
        name: user.name
      },
      {
        $set: {
          autorepricer_run: !autorepricerStatus
        }
      }
  ).then(() => {
    if (autorepricers[user.email]){
      responseJSON.success = true;
      autorepricers[user.email].trigger(autorepricerStatus);
    }
    responseJSON.status = (!autorepricerStatus) ? 'active' : 'paused';

    res.end(JSON.stringify(responseJSON));
  })
      .catch(() => {
        res.end(JSON.stringify(responseJSON));
      });
});

//AUTOREPRICER






//PROFILE

app.get('/profile', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  if (userInfo == null) {
    return res.render('404');
  }
  userInfo.showRequisitesNotification = false;
  res.render('profile/edit', {
    mainInfo: req.session,
    userInfo,
  });
});

app.post('/profile', auth, async function (req, res) {
  const {
    name,
    lastname,
    sellerName,
    ozonApiKey,
    ozonClientId,
  } = req.body;
  let resp = await req.db.collection('user').find({
    email: req.session.tsUser
  }).toArray();

  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $set: {
          name,
          lastname,
          sellerName,
          ozonApiKey,
          ozonClientId,
        }
      });
  req.method = 'GET';
  res.redirect('/items?tab=settings');
});

app.post('/profile/password-reset', auth, async function (req, res) {
  const {
    password,
    newpassword,
    passwordcopy
  } = req.body;
  let resp = await req.db.collection('user').findOne({
    email: req.session.tsUser,
    password
  });

  let goodMessage = {
        message: "Пароль успешно изменён",
        updated: true
      },
      badMessage = {
        message: "Проверьте правильность ввода",
        updated: false
      };

  if (resp == null || newpassword !== passwordcopy || newpassword.length < 4) {
    return res.end(JSON.stringify(badMessage));
  }

  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $set: {
          password: newpassword
        }
      });
  return res.end(JSON.stringify((result.result.nModified ? goodMessage : badMessage)));
});

app.get('/profile/remove-recurrent', auth, async function (req, res) {
  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $unset: {
          payment_method_id: ""
        }
      });
  return res.end(JSON.stringify(result));
});


//PROFILE






//SUBSCRIPTION


app.get('/subscription', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);
  if (userInfo == null) {
    return res.render('404');
  }

  if (userInfo.subscription_status == config.orderStatus.pending)
    return res.redirect('/subscription/payment/result');

  userInfo.hadTest = await userHadTest(req, userInfo.ozonClientId, userInfo.ozonApiKey);
  let subscriptions = await req.db.collection('subscriptions').find({}).toArray();
  subscriptions = [...subscriptions].filter(subs => Number(subs.price));
  let subscription = await req.db.collection('subscriptions').findOne({ _id: userInfo.subscription_id });
  if (subscription != null) {
    userInfo.subscription_name = subscription.name;
    userInfo.subscription_date = userInfo.subscription_expire_date.toISOString().split('T')[0];
  }
  res.render('subscription/info', {
    mainInfo: req.session,
    userInfo,
    subscriptions
  });
});

app.get('/subscription/payment', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  const { subscriptionType, recurrent } = req.query;

  var sendReceiptTo = req.query.sendReceiptTo || userInfo.email;

  if (!subscriptionType) {
    return res.render('404');
  }

  /*
  if (userSubscriptionLevel(user._id, req) > 1) {
    return res.redirect('/subscription/payment/result');
  }*/

  const subscription = await req.db.collection('subscriptions').findOne({ name: subscriptionType });
  if (subscription == null) {
    return res.render('404');
  }

  const idempotenceKey = await userMakeOrder(req, userInfo, subscription);

  if (subscription.name == "Test" || subscription.name == "Тестовый")
    return res.redirect('/subscription/payment/result');

  yandexCheckout.createPayment({
    "amount": {
      "value": subscription.price,
      "currency": "RUB"
    },
    "confirmation": {
      "type": "embedded"
    },
    "capture": true,
    "description": "Оплата подписки " + subscription.name,
    "receipt": {
      "customer": {
        "email": userInfo.email
      },
      "items": [{
        "description": "Подписка "+subscription.name,
        "quantity": "1.0",
        "amount": {
          "value":subscription.price,
          "currency": "RUB"
        },
        "vat_code": 1
      }],
      "email": sendReceiptTo
    },
    "save_payment_method": Boolean(recurrent)
  }, idempotenceKey)
      .then(function (result) {
        console.log({ payment: result });
        req.db.collection('orders').updateOne({ '_id': idempotenceKey },
            {
              $set: {
                paymentID: result.id,
                status: result.status
              }
            });
        res.render('subscription/payment', {
          userInfo,
          confirmation_token: result.confirmation.confirmation_token
        });
      })
      .catch(function (err) {
        console.error(err);
        req.db.collection('orders').updateOne({ '_id': idempotenceKey }, {
              $set: {
                status: config.orderStatus.canceled
              }
            }
        ).then(() => res.redirect('/subscription/payment/result'));
      })
});

app.get('/subscription/payment/result', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  if (!(userInfo && userInfo.last_order_id)) {
    return res.render('404');
  }

  const lastOrder = await req.db.collection('orders').findOne({
    _id: userInfo.last_order_id
  });

  const subscription = await req.db.collection('subscriptions').findOne({
    _id: lastOrder.subscription_id
  });

  var status = lastOrder.status, message;

  if (subscription.name == "Test" || subscription.name == "Тестовый") {
    var hadTest = await userHadTest(req, userInfo.ozonClientId, userInfo.ozonApiKey);
    message = (hadTest) ? 'Ваш тестовый период был использован ранее' : 'Тестовый период успешно активирован!';
    status = hadTest ? config.orderStatus.canceled : config.orderStatus.succeeded;

    if (lastOrder.status == config.orderStatus.initial){
      req.db.collection('testTakings').insertOne({
        user_id: userInfo._id,
        ozonApiKey: userInfo.ozonApiKey,
        ozonClientId: userInfo.ozonClientId,
        datetime: Date.now(),
      })
      processOrder(req, lastOrder, status, config.subscription.testDaysCount);
    }
  } else {
    let orderResult = (status == config.orderStatus.succeeded) ? ' успешно выполнен!' : ' в процессе';
    if (status == config.orderStatus.canceled)
      orderResult = ' отклонен.';
    message = "Ваш заказ " + lastOrder.datetime + " " + orderResult;
  }

  res.render('subscription/result',
      {
        mainInfo: req.session,
        userInfo,
        info: { message, status }
      });
});

app.all('/service/yandex-checkout/', authService, async function (req, res) {
  res.status(200).end('');
  var {
    id,
    status,
    payment_method,
    metadata
  } = req.body.object;
  var amount = req.body.object.amount.value;
  var chatPayment = Boolean(metadata && metadata.chat);
  console.log("Payment" + id, status, payment_method, (chatPayment)? '(chat)': '');

  let order = await req.db.collection('orders').findOne({ paymentID: id });

  if (order.status == config.orderStatus.succeeded || order.status == config.orderStatus.canceled) {
    console.log("!!! Пользователь " + req.session.tsUser + " повторно пытался совершить платеж +" + id + " на сумму " + amount + "\n!!! Статус платежа: " + status);
    return;
  }


  if (payment_method.saved) {
    console.log("Подключен автоплатеж " + payment_method.id);
    savePaymentMethod(req, order.tsUser, payment_method.id, chatPayment);
  }

  processOrder(req, order, status, config.daysCount, chatPayment);
});

app.get('/.well-known/apple-developer-merchantid-domain-association', function (req, res) {
  let fileContent = fs.readFileSync('.well-known/apple-developer-merchantid-domain-association', 'utf8');
  res.set('Content-Type', 'text/plain');
  res.end(fileContent);
});

//SUBSCRIPTION



//OTHER

app.get('/contacts', async function (req, res) {
  let userInfo = await getUserInfo(req);
  res.render('contacts', {
    userInfo
  });
});


app.get('/aggregate', function (req, res) {
  importjs.aggregate(req, res);
});

app.get('/', guest, async function (req, res) {
  let priceChange = (await req.db.collection('counters').findOne({name: 'priceChange'})).value;
  res.render('index', {
    counters: {
      priceChange
    }
  });
});

//OTHER

app.listen(7777, function () {
  console.log("Started listening on port", 7777);
  checkPendingOrders();
  if (config.autoStart)
    initAutorepricers();
});

const checkPendingOrders = () => {
  var mongoConnection = {};
  const checkOrder = async (order) => {
    if(!(order.paymentID && order.paymentID.length))
      return;
    let result = await yandexCheckout.getPayment(order.paymentID);
    mongoConnection.db.collection('orders').updateOne({_id:order._id},
        {
          $set: {
            status: result.status
          }
        })
  };

  const checkOrders = async () => {
    let users = await mongoConnection.db.collection('user').find({}).toArray();

    users.forEach(async user => {
      if (user.last_order_id){
        let order = await mongoConnection.db.collection('orders').find({_id: user.last_order_id});
        if (order.status != config.orderStatus.canceled && order.status != config.orderStatus.succeeded)
          checkOrder(order)
      }
    });
  };

  expressMongoDb(config.dbURL)(mongoConnection, null, checkOrders);
};

const initAutorepricers = () => {
  var mongoConnection = {};
  let createAutorepricers = async () => {
    let users = await mongoConnection.db.collection('user').find({}).toArray();

    users.forEach(user => {
      if (user.email)
        autorepricers[user.email] = new Autorepricer(user.email, user.autorepricer_run);
    });
  };

  expressMongoDb(config.dbURL)(mongoConnection, null, createAutorepricers);
};

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

const ozonRequest = (url, clientID, apiKey, json, get = false) => {

  return new Promise((resolve, reject) => {
    const wrapResolve = (error, res, body) => {
      return resolve([error, res, body]);
    };
    let options = {
      url: 'http://api-seller.ozon.ru' + url,
      headers: {
        'Host': 'api-seller.ozon.ru',
        'Client-Id': parseInt(clientID),
        'Api-Key': apiKey,
        'Content-Type': 'application/json'
      },
      json
    };

    if (get)
      request.get(options, wrapResolve);
    else
      request.post(options, wrapResolve);
  });
};

const userLogFileName = username => 'log/' + username + '.log';

const logIntoFile = (username, message, status = config.messageStatus.message) => {
  var today = new Date();
  var dateTime =
      (today.getDate().toString().length < 2 ? '0' + today.getDate() : today.getDate()) + '.' +
      (today.getMonth().toString().length < 2 ? '0' + (today.getMonth() + 1) : today.getMonth() + 1) + '.' +
      today.getFullYear() + ' ' +
      (today.getHours().toString().length < 2 ? '0' + today.getHours() : today.getHours()) + ':' +
      (today.getMinutes().toString().length < 2 ? '0' + today.getMinutes() : today.getMinutes()) + ':' +
      (today.getSeconds().toString().length < 2 ? '0' + today.getSeconds() : today.getSeconds())
  ;
  let filename = userLogFileName(username);
  if (!fs.existsSync(filename))
    fs.writeFileSync(filename, "");
  fs.appendFileSync(filename, dateTime + "|" + status + "|" +message + "\n", 'utf-8');
}

const userSubscriptionLevel = async (req, email) => {
  let user = await req.db.collection('user').findOne({
    email
  });

  if (user.subscription_expire_date > Date.now()) {
    let subscription = await req.db.collection('subscriptions').findOne({
      _id: user.subscription_id
    });

    return subscription.level;
  }
  else return 0;
}

const userHadTest = async(req, ozonClientId, ozonApiKey) => {
  let hadTest = await req.db.collection('testTakings').findOne({
    ozonApiKey,
    ozonClientId
  });

  return Boolean(hadTest != null);
}

const maxArtCount = async (subscription_id, req) => {
  let subscription = await req.db.collection('subscriptions').findOne({
    _id: subscription_id
  });

  if (subscription == null)
    return 0;


  return subscription.maxArtCount || -1;
}

const userCanAddItems = async (req, user=null) => {
  if(user==null)
    user = await req.db.collection('user').findOne({email: req.session.tsUser});

  let maxCount = await maxArtCount(user.subscription_id, req);

  if (maxCount < 0) return true;
  if (maxCount == 0) return false;

  let actualItems = await req.db.collection('ozonItems').find({
    tsUser: user.email
  }).toArray();
  let actualCount = actualItems.length;

  return Boolean(maxCount >= actualCount + 1);
}

const processOrder = async(req, order, status, days=config.subscription.daysCount, chatPayment) => {
  req.db.collection('orders').updateOne({ _id: order._id }, {
    $set: {
      status
    }
  });

  if(status == config.orderStatus.canceled){
    req.db.collection('user').updateOne({ email: order.tsUser }, {
      $unset: ((chatPayment) ? { chat_payment_method_id: "" } : { payment_method_id: "" })
    });
  }

  if ((status != config.orderStatus.succeeded))
    return;

  var subscription_expire_date = new Date();
  subscription_expire_date.setDate(subscription_expire_date.getDate() + days);

  let user = await req.db.collection('user').updateOne({ email: order.tsUser }, {
    $set: ((chatPayment) ? {
    chat_expire_date: subscription_expire_date
    } : {
        subscription_id: order.subscription_id,
        subscription_expire_date
      })
  });

  console.log("Пользователь " + req.session.tsUser + " оплатил и подключил подписку до " + subscription_expire_date.getDate()+" числа");

  if(chatPayment) return;
  delete autorepricers[user.email];
  autorepricers[user.email] = new Autorepricer(user.email, user.autorepricer_run);
}

const savePaymentMethod = async (req, email, payment_method_id, chatPayment) => {
  
  req.db.collection('user').updateOne({ email }, {
    $set: ((chatPayment) ? { chat_payment_method_id: payment_method_id } : { payment_method_id })
  });
}

const userAutoPayment = async (req, email) => {
  let user = await req.db.collection('user').findOne({ email });
  if (!user || !user.payment_method_id || !user.last_order_id)
    return;

  console.log("["+email+"] Проверяем возможность автоплатежа");

  let last_order = await req.db.collection('orders').findOne({ _id: user.last_order_id });
  if (last_order.status != config.orderStatus.succeeded && last_order.status != config.orderStatus.canceled)
    return console.log("[" + email + "] Ждём завершения заказа " + last_order._id);

  let subscription = await req.db.collection('subscriptions').findOne({ _id: last_order.subscription_id});

  let idempotenceKey = await userMakeOrder(req, user, subscription);

  yandexCheckout.createPayment({
    "amount": {
      "value": subscription.price,
      "currency": "RUB"
    },
    "capture": true,
    "payment_method_id": user.payment_method_id,
    "description": "Ежемесячная оплата подписки " + subscription.name,
    "receipt": {
      "customer": {
        "email": user.email
      },
      "items": [
        {
          "description": "Подписка " + subscription.name,
          "quantity": "1.0",
          "amount": {
            "value": subscription.price,
            "currency": "RUB"
          },
          "vat_code": 1
        }
      ],
      "email": user.email
    }
  }, idempotenceKey)
      .then(function (result) {
        console.log({ payment: result });
        req.db.collection('orders').updateOne({ '_id': idempotenceKey },
            {
              $set: {
                paymentID: result.id,
                status: result.status
              }
            });

        if(result.paid){
          console.log("[" + email + "] Автоплатеж успешно совершён. Доступ к подписке " + subscription.name + " активен!");
          logIntoFile(email, "Автоплатеж успешно совершён. Доступ к подписке "+subscription.name+" активен!", config.messageStatus.success);
        }else{
          console.log("[" + email + "] Автоплатеж отклонён");
          logIntoFile(email, "Автоплатеж отклонён", config.messageStatus.error);
        }

        processOrder(req, last_order, result.status);
      })
      .catch(function (err) {
        console.error(err);
        req.db.collection('orders').updateOne({ '_id': idempotenceKey }, {
              $set: {
                status: config.orderStatus.canceled
              }
            }
        ).then(() => res.redirect('/subscription/payment/result'));
      })
}

const userMakeOrder = async (req, user, subscription) => {
  const order = await req.db.collection('orders').insertOne({
    tsUser: user.email,
    datetime: Date.now(),
    amount: subscription.price,
    subscription_id: subscription._id,
    status: config.orderStatus.initial,
    paymentID: ''
  });

  req.db.collection('user').updateOne({
        _id: user._id
      },
      {
        $set: {
          last_order_id: order.insertedId
        }
      });

  return order.insertedId;
}

const getUserInfo = async (req) => {
  let userInfo = await req.db.collection('user').findOne({
    email: req.session.tsUser
  });

  userInfo.showRequisitesNotification = !Boolean(userInfo.ozonApiKey && userInfo.ozonClientId && userInfo.ozonApiKey.length && userInfo.ozonClientId.length);
  let subscription = await req.db.collection('subscriptions').findOne({ _id: userInfo.subscription_id });

  if (subscription != null) {
    userInfo.subscription_name = subscription.name;
    userInfo.subscription_date = userInfo.subscription_expire_date.toISOString().split('T')[0];
    var date = userInfo.subscription_expire_date;
    var formattedDate =
        (date.getDate().toString().length < 2 ? '0' + date.getDate() : date.getDate()) + '.' +
        (date.getMonth().toString().length < 2 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '.' +
        date.getFullYear()
    ;
    userInfo.subscription_date = formattedDate;
  }

  return userInfo;
}