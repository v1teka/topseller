let reports = require('../reports');
let session = require('../session');

exports.sales = function (req, res) {
    console.log(Date.now() + ' ' + req.session.username + ' salesByDay');
    reports.salesByDay(req.session.username, function (result) {
        if (result) {
            res.send(result)
        } else {
            res.send({});
        }
    });
};

exports.margin = function (req, res) {
    console.log(Date.now() + ' ' + req.session.username + ' marginByDay');
    reports.marginByDay(req.session.username, function (result) {
        if (result) {
            res.send(result)
        } else {
            res.send({});
        }
    });

    // console.log(Date.now() + ' marginByGoods');
    // reports.marginByGoods(req.session.username, function (result) {
    //     if (result) {
    //         res.send(result)
    //     } else {
    //         res.send({});
    //     }
    // });
};

exports.profit = function (req, res) {
    console.log(Date.now() + ' ' + req.session.username + ' profitByDay');
    reports.profitByDay(req.session.username, function (result) {
        if (result) {
            res.send(result)
        } else {
            res.send({});
        }
    });
};

exports.orders = function (req, res) {
    console.log(Date.now() + ' ' + req.session.username + ' orders');
    reports.orders(req.session.username, function (result) {
        if (result) {
            res.send(result)
        } else {
            res.send({});
        }
    });
};

exports.common = function (req, res) {
    console.log(Date.now() + ' ' + req.session.username + ' commmonData');
    reports.commonData(req.session.username, function (result) {
        if (result) {
            res.send(result)
        } else {
            res.send({});
        }
    });
};