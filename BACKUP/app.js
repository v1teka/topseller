let express = require("express");
let path = require("path");
let bodyParser = require("body-parser");
let user = require('./user');
let exprSession = require('express-session');
let importjs = require('./import');
let goods = require('./goods');
let session = require('./session');
let auth = require('./middleware/auth').auth;
let authAdmin = require('./middleware/authAdmin').authAdmin;
let guest = require('./middleware/guest').guest;
let registerRequest = require('./requests/register').registerRequest;
let loginRequest = require('./requests/login').loginRequest;
let expressMongoDb = require('express-mongo-db');
let swig = require('swig');


const app = express();
const import_route = require('./routes/import.route');
const reports_route = require('./routes/report.route');
// let sessions;

app.use(expressMongoDb('mongodb://dmi:dmi123@ds317808.mlab.com:17808/freelance101'));
app.use(express.static("public"));
app.use(exprSession({
  secret: 'topse11er'
}));
app.use('/collect', import_route);
app.use('/report', reports_route);
// SWIG


app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', `${__dirname}/views`);
app.set('view cache', false);
swig.setDefaults({
  cache: false
});

//SWIG

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());



// AUTHORIZATION
app.get('/login', guest, function (req, res) {
  res.render('login');
});
app.get('/register', guest, function (req, res) {
  res.render('register');
});
app.post('/register', guest, function (req, res) {
  registerRequest(req, res);
});
app.post('/login', guest, function (req, res) {
  loginRequest(req, res);
});
app.get('/logout', auth, function (req, res) {
  req.session.destroy;
  req.session.destroy();
  return res.redirect('/login');
});

// AUTHORIZATION

app.get('/', auth, function (req, res) {
  res.render('index', {
    mainInfo: req.session
  });
});
app.get('/store', auth, async (req, res) => {
  try {
    const { tsUser } = req.session
    const items = await req.db.collection('goods').find({
      tsUser
    })
      .sort({
        email: 1
      }).toArray()
    
    res.render('store', {
      mainInfo: req.session,
      items
    })
  } catch (error) {
    res.send(`Error occured: ${error}`)
  }
})
app.get('/store', auth, async function (req, res) {
  console.log(req.session.tsUser)
  let resp = await req.db.collection('goods').find({
      tsUser: req.session.tsUser
    })
    .sort({
      email: 1
    }).toArray();
  console.log(resp)
  res.render('store', {
    mainInfo: req.session,
    items: resp
  });
});
app.get('/categories', auth, async function (req, res) {
  let resp = await req.db.collection('categories').find({
      tsUser: req.session.tsUser
    }).toArray();
  console.log(resp);
  res.render('categories', {
    mainInfo: req.session,
    categories: resp
  });
});
app.get('/additem', auth, async (req, res) => {
  let resp1 = await req.db.collection('categories').find({
    tsUser: req.session.tsUser
  }).toArray();
  let resp = await req.db.collection('items').find({
      tsUser: req.session.tsUser
    })
    .sort({
      email: 1
    }).toArray();
  console.log(resp)
  res.render('additem', {
    mainInfo: req.session,
    items: resp,
    categories: resp1
  });
});
app.get('/items', auth, async function (req, res) {
  let resp1 = await req.db.collection('categories').find({
    tsUser: req.session.tsUser
  }).toArray();
  let resp = await req.db.collection('items').find({
      tsUser: req.session.tsUser
    })
    .sort({
      email: 1
    }).toArray();
  console.log(resp)
  res.render('items', {
    mainInfo: req.session,
    items: resp,
    categories: resp1
  });
});
app.get('/items/:name', auth, async function (req, res) {
  const { name } = req.params
  let resp = await req.db.collection('items').find({
      tsUser: req.session.tsUser,
      category: name
    })
    .sort({
      email: 1
    }).toArray();
  console.log(resp)
  res.render('items', {
    mainInfo: req.session,
    items: resp
  });
});
app.post('/categories', auth, async function (req, res) {
  const {
    name
  } = req.body

  let check = await req.db.collection('categories').findOne({
    tsUser: req.session.tsUser,
    name
  })
  if (check == null) {
    let resp = await req.db.collection('categories').insertOne({
      tsUser: req.session.tsUser,
      name
    });
  } else {
    let resp = await req.db.collection('categories').find({
      tsUser: req.session.tsUser
    }).toArray();

    return res.render('categories', {
      mainInfo: req.session,
      categories: resp,
      error: "Категория уже существует"
    });
  }
  return res.redirect('/categories');
});
app.post('/items', auth, async function (req, res) {
  const {
    name,
    image,
    ozon,
    wildberries,
    own,
    price,
    category
  } = req.body

  let check = await req.db.collection('items').findOne({
    tsUser: req.session.tsUser,
    name
  });
  if (check == null) {
    let resp = await req.db.collection('items').insertOne({
      tsUser: req.session.tsUser,
      name,
      image,
      ozon,
      wildberries,
      own,
      price,
      category,
      count: 0
    });
  } else {
    let resp = await req.db.collection('items').update({
      product_name: req.body.title,
      tsUser: req.session.tsUser
    }, {
      name,
      image,
      ozon,
      wildberries,
      own,
      price,
      category
    });
    console.log(resp);
  }
  return res.redirect('/items');
});
app.post('/store', auth, async function (req, res) {
  let check = await req.db.collection('goods').findOne({
    product_name: req.body.title,
    tsUser: req.session.tsUser
  });
  if (check == null) {
    return res.redirect('/additem')
    let resp = await req.db.collection('goods').insertOne({
      date: req.body.date,
      product_name: req.body.title,
      tsUser: req.session.tsUser,
      count: req.body.count
    });
  } else {
    let count = parseInt(check.count) + parseInt(req.body.count);
    let resp = await req.db.collection('goods').update({
      product_name: req.body.title,
      tsUser: req.session.tsUser
    }, {
      date: req.body.date,
      product_name: req.body.title,
      count: count,
      tsUser: req.session.tsUser
    });
  }
  return res.redirect('/store');
});
app.post('/storediv', auth, async function (req, res) {
  let check = await req.db.collection('goods').findOne({
    product_name: req.body.title,
    tsUser: req.session.tsUser
  });
  if (check == null) {
    let resp = await req.db.collection('goods').find({
        tsUser: req.session.tsUser
      })
      .sort({
        email: 1
      }).toArray();
    await req.db.collection('user').remove({});
    return res.render('store', {
      mainInfo: req.session,
      items: resp,
      error: "Продукт не существует"
    });
  } else {
    let count = parseInt(check.count) - parseInt(req.body.count);
    let resp = await req.db.collection('goods').update({
      product_name: req.body.title,
      tsUser: req.session.tsUser
    }, {
      date: req.body.date,
      product_name: req.body.title,
      count: count,
      tsUser: req.session.tsUser
    });
  }

  return res.redirect('/store');
});
app.get('/autocompleteStore', auth, async function (req, res) {
  let resp = await req.db.collection('goods').find({
      tsUser: req.session.tsUser
    })
    .sort({
      email: 1
    }).toArray();
  return res.json(resp);
});

// ADMIN

app.get('/users', authAdmin, async function (req, res) {
  let resp = await req.db.collection('user').find({}).toArray();
  res.render('users', {
    mainInfo: req.session,
    users: resp
  });
});

// ADMIN

// TESTING

app.get('/all-users', async function (req, res) {
  let resp = await req.db.collection('user').find({}).toArray();
  console.log(resp)
  res.json(resp);
});
app.get('/goodz', async function (req, res) {
  let resp = await req.db.collection('goods').find({}).toArray();
  console.log(resp)
  res.json(resp);
});
app.get('/goodsByDay', async function (req, res) {
  let resp = await req.db.collection('goodsByDay').find({}).toArray();
  console.log(resp)
  res.json(resp);
});

app.get('/salez', async function (req, res) {
  let resp = await req.db.collection('sales').find({}).toArray();
  console.log(resp)
  res.json(resp);
});

// TESTING

app.get('/aggregate', function (req, res) {
  importjs.aggregate(req, res);
});

app.get('/goods/byUser', function (req, res) {
  if (session.validated) {
    console.log(session);
    console.log(session.tsUser + ' goods');
    goods.byUser(session.tsUser).then((result) => {
      if (result) {
        res.send(result);
      };
    });
  } else {
    res.send('Не верный логин или пароль')
  }
});

app.get('/goods', function (req, res) {
  if (session.validated) {
    res.sendFile(__dirname + '/html/goods.html');
  } else {
    res.send('Ошибка авторизации');
  };
});

app.get('/stock/all', function (req, res) {
  goods.byUser(session.tsUser).then((result) => {
    if (result) {
      res.send(result);
    };
  });
});

app.get('/stock', function (req, res) {
  if (session.validated) {
    res.sendFile(__dirname + '/html/stock.html');
  } else {
    res.send('Ошибка авторизации');
  };
});

// app.get('/collect/all', function (req, res) {
//     if(session.validated){
//         res.send(importjs.collectAll(req, res));
//     }
//     else{
//         res.send('Ошибка авторизации');
//     };
// });

app.get('/collect/totals', function (req, res) {
  if (session.validated) {
    res.send(importjs.aggregate(req, res));
  } else {
    res.send('Ошибка авторизации');
  };
});

app.listen(7777, function () {
  console.log("Started listening on port", 7777);
});