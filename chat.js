let express = require("express");
let path = require("path");
let bodyParser = require("body-parser");
let user = require('./user');
let exprSession = require('express-session');
let importjs = require('./import');
let goods = require('./goods');
let session = require('./session');
let auth = require('./middleware/auth').auth;
let authAdmin = require('./middleware/authAdmin').authAdmin;
let authService = require('./middleware/authService').authService;
let guest = require('./middleware/guest').guest;
let registerRequest = require('./requests/register').registerRequest;
let loginRequest = require('./requests/login').loginRequest;
let expressMongoDb = require('express-mongo-db');
let swig = require('swig');


const app = express();
const import_route = require('./routes/import.route');
const reports_route = require('./routes/report.route');
const request = require('request');
const fs = require('fs');
const yandexCheckout = require('yandex-checkout')('649384', 'live_PSPk8KVXARyqNiJLVKIl_TSwKDcHp9eG38gD91kyUeo');

const config = {
  dbURL: 'mongodb://dmi:dmi123@ds317808.mlab.com:17808/freelance101',
  orderStatus: {
    initial: 'initial',
    pending: 'pending',
    succeeded: 'succeeded',
    canceled: 'canceled',
  },
  subscription:{
    daysCount: 30
  },
  secret: 'topse11er'
};

// let sessions;

app.use(expressMongoDb(config.dbURL));
app.use(express.static("public"));
app.use(exprSession({
  secret: config.secret
}));

// SWIG

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', `${__dirname}/views`);
app.set('view cache', false);
swig.setDefaults({
  cache: false
});

//SWIG

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// AUTHORIZATION

app.get('/login', guest, function (req, res) {
  res.render('login');
});
app.get('/items', auth, function (req, res) {
  res.redirect('/subscription');
});
app.get('/register', guest, function (req, res) {
  res.render('register');
});
app.post('/register', guest, function (req, res) {
  registerRequest(req, res);
});
app.post('/login', guest, function (req, res) {
  loginRequest(req, res);
});
app.get('/logout', auth, function (req, res) {
  req.session.destroy;
  req.session.destroy();
  return res.redirect('/login');
});

// AUTHORIZATION


// TESTING

app.get('/api/user/ozon', async function (req, res) {
  const { user_email, password } = req.query;

  let resp = await req.db.collection('user').findOne({
    email: user_email
  });

  if (resp == null)
    return res.status(404).end('{}');

  if (resp.password != password)
    return res.status(403).end('Access denied');

  let clientOzonData = {
    email: resp.email,
    ozonClientId: resp.ozonClientId,
    ozonApiKey: resp.ozonApiKey
  }
  res.json(clientOzonData);
});

app.get('/api/user/subscription', async function (req, res) {
  const { user_email, password } = req.query;

  let userInfo = await getUserInfo(req, user_email);

  if (userInfo == null)
    return res.status(404).end('{}');

  if (userInfo.password != password)
    return res.status(403).end('Access denied');

  let clientSubscriptionData = {
    email: user_email,
    chat_subscription: userInfo.chat_subscription
  }
  res.json(clientSubscriptionData);
});

// TESTING

//PROFILE

app.get('/profile', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  if (userInfo == null) {
    return res.render('404');
  }
  userInfo.showRequisitesNotification = false;
  res.render('profile/chat_edit', {
    mainInfo: req.session,
    userInfo,
  });
});

app.post('/profile', auth, async function (req, res) {
  const {
    name,
    lastname,
    ozonApiKey,
    ozonClientId,
  } = req.body;
  let resp = await req.db.collection('user').find({
    email: req.session.tsUser
  }).toArray();

  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $set: {
          name,
          lastname,
          ozonApiKey,
          ozonClientId,
        }
      });
  req.method = 'GET';
  res.redirect('/items?tab=settings');
});

app.post('/profile/password-reset', auth, async function (req, res) {
  const {
    password,
    newpassword,
    passwordcopy
  } = req.body;
  let resp = await req.db.collection('user').findOne({
    email: req.session.tsUser,
    password
  });

  let goodMessage = {
        message: "Пароль успешно изменён",
        updated: true
      },
      badMessage = {
        message: "Проверьте правильность ввода",
        updated: false
      };

  if (resp == null || newpassword !== passwordcopy || newpassword.length < 4) {
    return res.end(JSON.stringify(badMessage));
  }

  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $set: {
          password: newpassword
        }
      });
  return res.end(JSON.stringify((result.result.nModified ? goodMessage : badMessage)));
});

app.get('/profile/remove-recurrent', auth, async function (req, res) {
  let result = await req.db.collection('user').updateOne({
        email: req.session.tsUser
      },
      {
        $unset: {
          chat_payment_method_id: ""
        }
      });
  return res.end(JSON.stringify(result));
});


//PROFILE






//SUBSCRIPTION


app.get('/subscription', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);
  if (userInfo == null) {
    return res.render('404');
  }

  if (userInfo.subscription_status == config.orderStatus.pending)
    return res.redirect('/subscription/payment/result');

  let subscription = await req.db.collection('chat_subscriptions').findOne({});

  res.render('chat_subscription/info', {
    mainInfo: req.session,
    userInfo,
    subscription
  });
});

app.get('/subscription/payment', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  const { subscriptionType, recurrent } = req.query;

  var sendReceiptTo = req.query.sendReceiptTo || userInfo.email;

  if (!subscriptionType) {
    return res.render('404');
  }

  const subscription = await req.db.collection('chat_subscriptions').findOne({/* name: subscriptionType*/ });
  if (subscription == null) {
    return res.render('404');
  }

  const idempotenceKey = await userMakeOrder(req, userInfo, subscription);

  if (subscription.name == "Test" || subscription.name == "Тестовый")
    return res.redirect('/subscription/payment/result');

  yandexCheckout.createPayment({
    "amount": {
      "value": subscription.price,
      "currency": "RUB"
    },
    "confirmation": {
      "type": "embedded"
    },
    "capture": true,
    "description": subscription.name,
    "receipt": {
      "customer": {
        "email": userInfo.email
      },
      "items": [{
        "description": "Подписка "+subscription.name,
        "quantity": "1.0",
        "amount": {
          "value":subscription.price,
          "currency": "RUB"
        },
        "vat_code": 1
      }],
      "email": sendReceiptTo
    },
    "metadata": {
      chat: true
    },
    "save_payment_method": Boolean(recurrent)
  }, idempotenceKey)
      .then(function (result) {
        console.log({ payment: result });
        req.db.collection('orders').updateOne({ '_id': idempotenceKey },
            {
              $set: {
                paymentID: result.id,
                status: result.status
              }
            });
        res.render('chat_subscription/payment', {
          userInfo,
          confirmation_token: result.confirmation.confirmation_token
        });
      })
      .catch(function (err) {
        console.error(err);
        req.db.collection('orders').updateOne({ '_id': idempotenceKey }, {
              $set: {
                status: config.orderStatus.canceled
              }
            }
        ).then(() => res.redirect('/subscription/payment/result'));
      })
});

app.get('/subscription/payment/result', auth, async function (req, res) {
  let userInfo = await getUserInfo(req);

  if (!(userInfo && userInfo.chat_last_order_id)) {
    return res.render('404');
  }

  const lastOrder = await req.db.collection('orders').findOne({
    _id: userInfo.chat_last_order_id
  });

  var status = lastOrder.status, message;

  let orderResult = (status == config.orderStatus.succeeded) ? ' успешно выполнен!' : ' в процессе';
  if (status == config.orderStatus.canceled)
    orderResult = ' отклонен.';
  message = "Ваш заказ " + lastOrder.datetime + " " + orderResult;

  res.render('chat_subscription/result',
      {
        mainInfo: req.session,
        userInfo,
        info: { message, status }
      });
});

//SUBSCRIPTION



//OTHER

app.get('/contacts', async function (req, res) {
  let userInfo = await getUserInfo(req);
  res.render('chat_contacts', {
    userInfo
  });
});


app.get('/aggregate', function (req, res) {
  importjs.aggregate(req, res);
});

app.get('/', guest, async function (req, res) {
  let priceChange = (await req.db.collection('counters').findOne({name: 'priceChange'})).value;
  res.render('chat_landing', {
    counters: {
      priceChange
    }
  });
});

//OTHER

app.listen(7778, function () {
  console.log("Started listening on port", 7778);
});

const processOrder = async(req, order, status, days=config.subscription.daysCount) => {
  req.db.collection('orders').updateOne({ _id: order._id }, {
    $set: {
      status
    }
  });

  if(status == config.orderStatus.canceled){
    req.db.collection('user').updateOne({ email: order.tsUser }, {
      $unset: {
        chat_payment_method_id: ""
      }
    });
  }

  if ((status != config.orderStatus.succeeded))
    return;

  var subscription_expire_date = new Date();
  subscription_expire_date.setDate(subscription_expire_date.getDate() + days);

  let user = await req.db.collection('user').updateOne({ email: order.tsUser }, {
    $set: {
      subscription_id: order.subscription_id,
      subscription_expire_date
    }
  });

  console.log("Пользователь " + req.session.tsUser + " оплатил и подключил подписку до " + subscription_expire_date.getDate()+" числа");

}

const savePaymentMethod = async (req, email, chat_payment_method_id) => {
  req.db.collection('user').updateOne({ email }, {
    $set: {
      chat_payment_method_id
    }
  });
}

const userAutoPayment = async (req, email) => {
  let user = await req.db.collection('user').findOne({ email });
  if (!user || !user.chat_payment_method_id || !user.chat_last_order_id)
    return;

  console.log("["+email+"] Проверяем возможность автоплатежа");

  let chat_last_order_id = await req.db.collection('orders').findOne({ _id: user.chat_last_order_id });
  if (chat_last_order_id.status != config.orderStatus.succeeded && chat_last_order_id.status != config.orderStatus.canceled)
    return console.log("[" + email + "] Ждём завершения заказа " + chat_last_order_id._id);

  let subscription = await req.db.collection('chat_subscriptions').findOne({});

  let idempotenceKey = await userMakeOrder(req, user, subscription);

  yandexCheckout.createPayment({
    "amount": {
      "value": subscription.price,
      "currency": "RUB"
    },
    "capture": true,
    "payment_method_id": user.chat_payment_method_id,
    "description": "Ежемесячная оплата '" + subscription.name+"'",
    "receipt": {
      "customer": {
        "email": user.email
      },
      "items": [
        {
          "description": subscription.name,
          "quantity": "1.0",
          "amount": {
            "value": subscription.price,
            "currency": "RUB"
          },
          "vat_code": 1
        }
      ],
      "email": user.email
    },
    "metadata": {
      chat: true
    }
  }, idempotenceKey)
      .then(function (result) {
        console.log({ payment: result });
        req.db.collection('orders').updateOne({ '_id': idempotenceKey },
            {
              $set: {
                paymentID: result.id,
                status: result.status
              }
            });

        if(result.paid){
          console.log("[" + email + "] Автоплатеж успешно совершён. Доступ к подписке " + subscription.name + " активен!");
          logIntoFile(email, "Автоплатеж успешно совершён. Доступ к подписке "+subscription.name+" активен!", config.messageStatus.success);
        }else{
          console.log("[" + email + "] Автоплатеж отклонён");
          logIntoFile(email, "Автоплатеж отклонён", config.messageStatus.error);
        }

        processOrder(req, chat_last_order_id, result.status);
      })
      .catch(function (err) {
        console.error(err);
        req.db.collection('orders').updateOne({ '_id': idempotenceKey }, {
              $set: {
                status: config.orderStatus.canceled
              }
            }
        ).then(() => res.redirect('/subscription/payment/result'));
      })
}

const userMakeOrder = async (req, user, subscription) => {
  const order = await req.db.collection('orders').insertOne({
    tsUser: user.email,
    datetime: Date.now(),
    amount: subscription.price,
    subscription_id: subscription._id,
    status: config.orderStatus.initial,
    paymentID: ''
  });

  req.db.collection('user').updateOne({
        _id: user._id
      },
      {
        $set: {
          chat_last_order_id: order.insertedId
        }
      });

  return order.insertedId;
}

const getUserInfo = async (req, email=null) => {
  let userInfo = await req.db.collection('user').findOne({
    email: email || req.session.tsUser
  });

  if(userInfo == null)
    return {};
    
  userInfo.showRequisitesNotification = !Boolean(userInfo.ozonApiKey && userInfo.ozonClientId && userInfo.ozonApiKey.length && userInfo.ozonClientId.length);
  userInfo.chat_subscription = Boolean(userInfo.chat_expire_date > Date.now());
  if (userInfo.chat_subscription) {
    var date = userInfo.chat_expire_date;
    var formattedDate =
        (date.getDate().toString().length < 2 ? '0' + date.getDate() : date.getDate()) + '.' +
        (date.getMonth().toString().length < 2 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '.' +
        date.getFullYear()
    ;
    userInfo.subscription_date = formattedDate;
  }

  return userInfo;
}